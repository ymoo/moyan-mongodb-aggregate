import { Schema, Types, ObjectId } from 'mongoose'
import {JSONSchema4,JSONSchema6,JSONSchema7} from 'json-schema'

type MatchTyep =
  | string
  | number
  | Date
  | null
  | undefined
  | boolean
  | Schema.Types.ObjectId
  | Types.ObjectId
  | ObjectId
  | Schema.Types.Boolean
  | ConditionalExpressionOperators
  | AggregationQuery
  | Array<any>
type ComparisonExpression = MatchTyep | Array<MatchTyep> | AggregationQuery
type Expressions =
  | ArithmeticExpressionOperators
  | ArrayExpressionOperators
  | BooleanExpressionOperators
  | ComparisonExpressionOperators
  | ConditionalExpressionOperators
  | CustomAggregationExpressionOperators
  | DataSizeExpressionOperators
  | DateExpressionOperators
  | MiscellaneousOperators
  | ObjectExpressionOperators
  | SetExpressionOperators
  | StringExpressionOperators
  | TextExpressionOperator
  | TrigonometryExpressionOperators
  | TypeExpressionOperators
  | OtherExpressionOperators

export interface SpecificationOperators {
  [key: string]: string | number | null | Expressions | Field | boolean | SpecificationOperators
}

/**
 * 其他选项
 */
export interface OtherExpressionOperators {
  $let?: {
    vars?: AggregationQuery
    in?: AggregationQuery
  }
  $options?:any
}

/**
 * https://docs.mongodb.com/manual/reference/operator/query-evaluation/
 */
export interface EvaluationQueryOperators{
  $expr?:AggregationQuery
  $jsonSchema?:AggregationQuery|JSONSchema4|JSONSchema6|JSONSchema7
  $mod?:number[]
  $regex?:string | RegExp
  $text?:    {
    $search:string,
    $language: string,
    $caseSensitive: string,
    $diacriticSensitive: boolean
  }
  $where:string | Function
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#arithmetic-expression-operators
 */
export interface ArithmeticExpressionOperators {
  $abs?: any
  $add?: any
  $ceil?: any
  $divide?: any
  $exp?: any
  $floor?: any
  $ln?: any
  $log?: any
  $log10?: any
  $mod?: any
  $multiply?: any
  $pow?: any
  $round?: any
  $sqrt?: any
  $subtract?: any
  $trunc?: any
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#array-expression-operators
 */
export interface ArrayExpressionOperators {
  $arrayElemAt?: any
  $arrayToObject?: any
  $concatArrays?: any
  $filter?: any
  $first?: any
  $in?: any
  $indexOfArray?: any
  $isArray?: any
  $last?: any
  $map?: any
  $objectToArray?: any
  $range?: any
  $reduce?: any
  $reverseArray?: any
  $size?: any
  $slice?: any
  $zip?: any
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#boolean-expression-operators
 */
export interface BooleanExpressionOperators {
  $and?: Array<AggregationQuery>
  $not?: Array<AggregationQuery>
  $or?: Array<AggregationQuery>
  $expr?: AggregationQuery
  $elemMatch?: AggregationQuery
  $in?: ComparisonExpression
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#comparison-expression-operators
 */
export interface ComparisonExpressionOperators {
  /**
   * -1 如果第一个值小于第二个。
   * 1  如果第一个值大于第二个。
   * 0  如果两个值相等。
   */
  $cmp?: ComparisonExpression

  /**
   * 等于
   */
  $eq?: ComparisonExpression

  /**
   * 大于
   */
  $gt?: ComparisonExpression

  /**
   * 大于等于
   */
  $gte?: ComparisonExpression

  /**
   * 小于
   */
  $lt?: ComparisonExpression

  /**
   * 小于等于
   */
  $lte?: ComparisonExpression

  /**
   * 不等
   */
  $ne?: ComparisonExpression

  /**
   * 在里面
   */
  $in?: ComparisonExpression
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#conditional-expression-operators
 */
export interface ConditionalExpressionOperators {
  $cond?:
    | {
        if: SpecificationOperators | ComparisonExpressionOperators | BooleanExpressionOperators
        then: SpecificationOperators | boolean | string | number
        else?: SpecificationOperators | boolean | string | number
      }
    | ComparisonExpression
  $ifNull?: ComparisonExpression
  $switch?: {
    branches?: Array<{
      case: SpecificationOperators | ComparisonExpressionOperators | BooleanExpressionOperators
      then: SpecificationOperators | boolean | string | number | null
    }>
    default?: SpecificationOperators | ComparisonExpressionOperators | boolean | string | number | null
  }
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#custom-aggregation-expression-operators
 */
export interface CustomAggregationExpressionOperators {
  $accumulator?: Field['$accumulator']
  $function?: Function
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#data-size-expression-operators
 */
export interface DataSizeExpressionOperators {
  $binarySize?: any
  $bsonSize?: any
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#date-expression-operators
 */
export interface DateExpressionOperators {
  $dateFromParts?: any
  $dateFromString?: any
  $dateToParts?: any
  $dateToString?: any
  $dayOfMonth?: any
  $dayOfWeek?: any
  $hour?: any
  $isoDayOfWeek?: any
  $isoWeek?: any
  $isoWeekYear?: any
  $millisecond?: any
  $minute?: any
  $month?: any
  $second?: any
  $toDate?: any
  $week?: any
  $year?: any
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#literal-expression-operator
 */
export interface LiteralExpressionOperator {
  $literal?: any
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#miscellaneous-operators
 */
export interface MiscellaneousOperators {
  $rand?: any
  $sampleRate?: any
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#miscellaneous-operators
 */
export interface ObjectExpressionOperators {
  $mergeObjects?: any
  $objectToArray?: any
}

/**
 * Set Expression Operators
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#set-expression-operators
 */
export interface SetExpressionOperators {
  $allElementsTrue?: any
  $anyElementTrue?: any
  $setDifference?: any
  $setEquals?: any
  $setIntersection?: any
  $setIsSubset?: any
  $setUnion?: any
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#string-expression-operators
 */
export interface StringExpressionOperators {
  $concat?: any
  $dateFromString?: any
  $dateToString?: any
  $indexOfBytes?: any
  $indexOfCP?: any
  $regexFind?: any
  $regexFindAll?: any
  $regexMatch?: any
  $replaceOne?: any
  $replaceAll?: any
  $rtrim?: any
  $split?: any
  $strLenBytes?: any
  $strLenCP?: any
  $strcasecmp?: any
  $substr?: any
  $substrBytes?: any
  $substrCP?: any
  $toLower?: any
  $toString?: any
  $trim?: any
  $toUpper?: any
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#text-expression-operator
 */
export interface TextExpressionOperator {
  $meta?: any
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#trigonometry-expression-operators
 */
export interface TrigonometryExpressionOperators {
  $sin?: any
  $cos?: any
  $tan?: any
  $asin?: any
  $acos?: any
  $atan?: any
  $atan2?: any
  $asinh?: any
  $acosh?: any
  $atanh?: any
  $sinh?: any
  $cosh?: any
  $tanh?: any
  $degreesToRadians?: any
  $radiansToDegrees?: any
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#type-expression-operators
 */
export interface TypeExpressionOperators {
  $convert?: any
  $isNumber?: any
  $toBool?: any
  $toDate?: any
  $toDecimal?: any
  $toDouble?: any
  $toInt?: any
  $toLong?: any
  $toObjectId?: any
  $toString?: any
  $type?: any
}

export interface AggregationQuery extends ComparisonExpressionOperators, BooleanExpressionOperators, ConditionalExpressionOperators, DateExpressionOperators {
  [key: string]:
    | MatchTyep
    | Array<MatchTyep>
    | Array<AggregationQuery>
    | ComparisonExpressionOperators
    | BooleanExpressionOperators
    | string
    | boolean
    | undefined
    | Types.ObjectId
    | Schema.Types.ObjectId
    | OtherExpressionOperators
    | ArithmeticExpressionOperators
    | ConditionalExpressionOperators
    | DateExpressionOperators
    | EvaluationQueryOperators
}

/**
 * https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#accumulators--in-other-stages
 */
export interface Field {
  /**
   * 返回用户定义的累加器函数的结果。
   */
  $accumulator?: {
    init: Function
    initArgs: Array<any> // Optional
    accumulate: Function
    accumulateArgs: Array<any>
    merge: Function
    finalize: Function // Optional
    lang: 'js'
  }

  /**
   * 返回每个组的唯一表达式值数组。数组元素的顺序未定义。
   */
  $addToSet?: string

  /**
   * 返回数值的平均值。忽略非数字值。
   */
  $avg?: string | ArithmeticExpressionOperators | ArrayExpressionOperators

  /**
   * 从每个组的第一个文档返回一个值。仅当文档处于定义的顺序时才定义顺序。与$first数组运算符不同
   */
  $first?: any

  /**
   * 从每个组的最后一个文档返回一个值。仅当文档处于定义的顺序时才定义顺序。与$last数组运算符不同。
   */
  $last?: any

  /**
   * 返回每个组的最高表达式值。
   */
  $max?: any

  /**
   * 返回通过组合每个组的输入文档创建的文档。
   */
  $mergeObjects?: any

  /**
   * 返回每个组的最低表达式值。
   */
  $min?: any

  /**
   * 返回每个组的表达式值数组。
   */
  $push?: any

  /**
   * 返回输入值的总体标准差。
   */
  $stdDevPop?: any

  /**
   * 返回输入值的样本标准差。
   */
  $stdDevSamp?: any

  /**
   * 返回数值的总和。忽略非数字值。
   */
  $sum?: any
}

export interface Stage {
  /**
   * 向文档添加新字段。与$project类似，$addFields重塑了流中的每个文档;具体而言，通过向输出文档添加新字段，该文档包含输入文档和新添加字段中的现有字段。$set是的别名$addFields。
   */
  $addFields?: SpecificationOperators | Field

  /**
   * 根据指定的表达式和存储段边界将传入文档分类为称为存储段的组。
   */
  $bucket?: any

  /**
   * 根据指定的表达式将传入的文档分类为特定数量的组(称为存储桶)。自动确定存储桶边界，以尝试将文档均匀地分配到指定数量的存储桶中。
   */
  $bucketAuto?: any

  /**
   * 返回有关集合或视图的统计信息
   */
  $collStats?: any

  /**
   * 返回聚合管道此阶段的文档数量计数。
   */
  $count?: any

  /**
   * 在同一阶段的同一组输入文档上处理多个聚合管道。支持在一个阶段中创建能够表征多维或多面数据的多面聚合。
   */
  $facet?: any

  /**
   * 基于与地理空间点的接近度返回有序的文档流。将$match，$sort和$limit的功能合并到地理空间数据中。输出文档包括附加距离字段，并且可以包括位置标识符字段。
   * https://docs.mongodb.com/manual/reference/operator/aggregation/geoNear/#mongodb-pipeline-pipe.-geoNear
   */
  $geoNear?: {
    near: {
      type: 'Point'
      coordinates: [number, number]
    }
    distanceField?: string
    maxDistance?: number
    query?: AggregationQuery
    includeLocs?: string
    spherical?: boolean
    key?: string
    minDistance?: number
    uniqueDocs?: boolean
  }

  /**
   * 对集合执行递归搜索。对于每个输出文档，添加一个新的 array 字段，该字段包含该文档的递归搜索的遍历结果。
   */
  $graphLookup?: any

  /**
   * 按指定的标识符表达式对文档进行分组，并将累加器表达式(如果指定)应用于每个 group。消耗所有输入文档，并为每个不同的 group 输出一个文档。输出文档仅包含标识符字段，如果指定，则包含累积字段。
   */
  $group?: {
    _id: string | Field | null | { [key: string]: any }
    [key: string]: string | Field | null | { [key: string]: any }
  }

  /**
   * 返回有关集合的每个索引的使用的统计信息。
   */
  $indexStats?: any

  /**
   * 将未修改的前 n 个文档传递给管道，其中 n 是指定的限制。对于每个输入文档，输出一个文档(对于前 n 个文档)或零文档(在前 n 个文档之后)。
   */
  $limit?: number

  /**
   * 列出所有活动时间已足够长以传播到system.sessions集合的会话。
   */
  $listSessions?: any

  /**
   * 对同一数据库中的另一个集合执行左外连接，以从“已连接”集合中过滤文档以进行处理
   * https://docs.mongodb.com/manual/reference/operator/aggregation/lookup/#mongodb-pipeline-pipe.-lookup
   */
  $lookup?:
    | {
        from: string
        localField: string
        foreignField: string
        as: string
        unwinding?: { preserveNullAndEmptyArrays: boolean }
      }
    | {
        from: string
        let?: {
          [key: string]: string
        }
        pipeline: Array<Stage>
        as: string
        unwinding?: { preserveNullAndEmptyArrays: boolean }
      }

  /**
   * 过滤文档流以仅允许匹配的文档未经修改地传递到下一个管道阶段。 $match使用标准的 MongoDB 查询。对于每个输入文档，输出一个文档(匹配)或零文档(不匹配)。
   */
  $match?: AggregationQuery

  $expr?: AggregationQuery

  /**
   * 将聚合管道的结果文档写入集合。该阶段可以将结果合并（插入新文档，合并文档，替换文档，保留现有文档，使操作失败，使用自定义更新管道处理文档）将结果合并到输出集合中。要使用该$merge阶段，它必须是管道中的最后一个阶段。
   * 4.2版中的新功能。
   */
  $merge?: any

  /**
   * 将聚合管道的结果文档写入集合。要使用$out阶段，它必须是管道中的最后一个阶段。
   */
  $out?: any

  /**
   * 返回集合的计划缓存信息。
   */
  $planCacheStats?: any

  /**
   * 重塑流中的每个文档，例如通过添加新字段或删除现有字段。对于每个输入文档，输出一个文档。另请参阅$unset删除现有字段。
   */
  $project?: SpecificationOperators | Field

  /**
   * 通过基于文档本身中存储的信息限制每个文档的内容来重塑流中的每个文档。包含$project和$match的功能。可用于实现字段级编辑。对于每个输入文档，输出一个或零个文档。
   */
  $redact?: any

  /**
   * 用指定的嵌入文档替换文档。该操作将替换输入文档中的所有现有字段，包括_id字段。指定嵌入在输入文档中的文档，以将嵌入的文档提升到顶部级别。$replaceWith是$replaceRoot阶段的别名 。
   */
  $replaceRoot?: any

  /**
   * 用指定的嵌入文档替换文档。该操作将替换输入文档中的所有现有字段，包括_id字段。指定嵌入在输入文档中的文档，以将嵌入的文档提升到顶部级别。$replaceWith是$replaceRoot阶段的别名 。
   */
  $replaceWith?: any

  /**
   * 从输入中随机选择指定数量的文档。
   */
  $sample?: any

  /**
   * 将新字段添加到文档。与$project相似，$set重塑流中的每个文档；具体而言，通过向输出文档添加新字段，该输出文档既包含输入文档中的现有字段，又包含新添加的字段。$set是$addFields阶段的别名。
   */
  $set?: Stage['$addFields']

  /**
   * 跳过前 n 个文档，其中 n 是指定的跳过编号，并将未修改的其余文档传递给管道。对于每个输入文档，输出零文档(对于前 n 个文档)或一个文档(如果在前 n 个文档之后)。
   */
  $skip?: number

  /**
   * 按指定的排序 key 重新排序文档流。只有顺序改变;文档保持不变。对于每个输入文档，输出一个文档。
   */
  $sort?:
    | {
        [key: string]: -1 | 1
      }
    | string

  /**
   * 根据指定表达式的 value 对传入文档进行分组，然后计算每个不同 group 中的文档计数。
   */
  $sortByCount?: any

  /**
   * 从文档中删除/排除字段。$unset是$project删除字段的阶段的别名。
   */
  $unset?: any

  /**
   * 从输入文档解构 array 字段以输出每个元素的文档。每个输出文档都使用元素 value 替换 array。对于每个输入文档，输出 n 个文档，其中 n 是 array 元素的数量，对于空 array 可以为零
   */
  $unwind?:
    | string
    | {
        path: string
        includeArrayIndex?: string
        preserveNullAndEmptyArrays?: boolean
      }
}

export type PipelineOptions = Array<Stage>
export default PipelineOptions

export as namespace Aggregations
