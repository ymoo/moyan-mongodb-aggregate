# moyan-mongodb-aggregate

## 描述
moyan-mongodb-aggregate.d.ts

这是一个mongodb 的 aggregate 辅助TypeScript 描述文件，让你在日常开发中快速编写的Pipeline语句，该描述文件满足大部分Pipeline语法。


## 安装

```
npm i moyan-mongodb-aggregate
```
## 演示


```TypeScript

import {PipelineOptions} from "moyan-mongodb-aggregate";

const pipeline:PipelineOptions = [
    {
        $match:{
            a:'x'
        }
    },
    {
        $lookup:{
            from:'User',
            localField:'user',
            foreignField:'_id',
            as:'user'
        }
    },
    {
        $unwind:{
            path:'$user',
            preserveNullAndEmptyArrays:true
        }
    },
    {
        $group:{
            _id:null,
            total:{
                $sum:1
            }
        }

    }
]
```
![](https://gitee.com/ymoo/moyan-mongodb-aggregate/raw/master/images/GIF.gif)


## 仓库
https://gitee.com/ymoo/moyan-mongodb-aggregate

